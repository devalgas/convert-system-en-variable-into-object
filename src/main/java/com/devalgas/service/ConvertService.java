package com.devalgas.service;

import com.devalgas.Pojo.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author devalgas kamga on 16/03/2023. 09:52
 */

@Service
@Slf4j
public class ConvertService {

    // Injection de la valeur de la variable d'environnement dans une propriété
    @Value("${MY_VARIABLE}")
    private Optional<String> myVariable;

    public void convertValueSysEnVarToObject() throws JsonProcessingException {
        if (myVariable.isPresent()) {
            log.info("myVariable : {}", myVariable);
            ObjectMapper objectMapper = new ObjectMapper();
            log.info("objectMapper : {}", objectMapper);
            JsonNode jsonNode = objectMapper.readTree(myVariable.get());
            User user = objectMapper.treeToValue(jsonNode, User.class);
//            User user = objectMapper.readValue(myVariable, User.class);
            log.info("user : {}", user.toString());
        }
    }
}
