package com.devalgas.Pojo;

import lombok.*;

/**
 * @author devalgas kamga on 16/03/2023. 10:04
 */

@Getter
@Setter
@Builder(setterPrefix = "with")
@AllArgsConstructor
@ToString
public class User {
    private String nom;
    private int age;
    private String ville;

    public User() {
    }
}
