package com.devalgas;

import com.devalgas.service.ConvertService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConvertSystemEnVariableIntoObjectApplication implements CommandLineRunner {

    private final ConvertService convertService;

    public ConvertSystemEnVariableIntoObjectApplication(ConvertService convertService) {
        this.convertService = convertService;
    }

    public static void main(String[] args) {
        SpringApplication.run(ConvertSystemEnVariableIntoObjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        convertService.convertValueSysEnVarToObject();
    }
}
