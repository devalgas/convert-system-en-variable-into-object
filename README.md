# README

## Environment variable creation

### Windows(powershell)

```powershell
[Environment]::SetEnvironmentVariable("MY_VARIABLE", '{"nom": "John", "age": 30, "ville": "Paris"}', "User")
```

### Linux(terminal)

```terminal
export MY_VARIABLE='{"nom": "John", "age": 30, "ville": "Paris"}'
```
reboot system

### Run

```terminal
git clone https://gitlab.com/devalgas/convert-system-en-variable-into-object.git
```

```terminal
cd convert-system-en-variable-into-object
```

```terminal
./mvnw package
```

```terminal
java -jar target/*.jar
```

##### Local

```terminal
./mvnw spring-boot:run
```
